<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <title>Field Theory Tools for Gravitational Wave Physics</title>
    
    <meta name="description" content="Presentation Slides made with impress.js" />
    <meta name="author" content="Gregor Kaelin" />

	<script type="text/x-mathjax-config">
	  MathJax.Hub.Config({
      "HTML-CSS": {
	  scale: 90,
	  styles: {
      '.MathJax_Display': {
      "margin": "20px 0"
      }
      }
	  },
	  TeX: { extensions: ["color.js"] },
	  });
	</script>
	<script type="text/javascript" src="extras/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

    <link href="css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  </head>

  <body class="impress-not-supported">

	<div class="fallback-message">
      <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
      <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
	  <p>
		$$
		\definecolor{desyOrange}{RGB}{242,142,0}
		\DeclareMathOperator{\Arcsinh}{arcsinh}
		\DeclareMathOperator{\Arctan}{arctan}
		\DeclareMathOperator{\arcosh}{arcosh}
		\newcommand\dd{{\mathrm d}}
		\newcommand{\bb}{{\mathbf b}}
		\newcommand{\bk}{{\mathbf k}}
		\newcommand{\bl}{{\mathbf l}}
		\newcommand{\bm}{{\mathbf m}}
		\newcommand{\bn}{{\mathbf n}}
		\newcommand{\bp}{{\mathbf p}}
		\newcommand{\bq}{{\mathbf q}}
		\newcommand{\br}{{\mathbf r}}
		\newcommand{\bw}{{\mathbf w}}
		\newcommand{\bx}{{\mathbf x}}
		\newcommand{\by}{{\mathbf y}}
		\newcommand{\bz}{{\mathbf z}}
		\newcommand{\bc}{{\mathbf c}}
		\newcommand{\bell}{\boldsymbol{\ell}}
		\newcommand\cO{\mathcal{O}}
		\newcommand{\cD}{\mathcal{D}}
		\newcommand\cA{\mathcal{A}}
		\newcommand\cM{\mathcal{M}}
		\newcommand\cN{\mathcal{N}}
		\newcommand\cE{\mathcal{E}}
		\newcommand\cS{\mathcal{S}}
		\newcommand\cP{\mathcal{P}}
		\newcommand\cF{\mathcal{F}}
		\newcommand\cL{\mathcal{L}}
		\newcommand\cH{\mathcal{H}}
		\newcommand\cU{\mathcal{U}}
		\newcommand\Mp{M_{\rm Pl}}
		$$
	  </p>
	</div>

	<div id="impress">

	  <div id="title" class="step" data-x="0" data-y="0">
		<h1>Field The<span class="orange">o</span>ry Tools for Gravitational Wave Physics</h1>
		<p class="margin-bottom30">Work with Christoph Dlapa, Zhengwen Liu, <br/>
		  Jakob Neef & Rafael Porto<br/><br/><br/><br/>
		</p>
		<img id="scatteringFig" src="figures/scattering.svg" alt="scattering">
		<img id="orbitFig" src="figures/orbit.svg" alt="orbit">
		<p id="author">
		  Gregor Kälin
		</p>
		<div class="margin-top50 margin-bottom30">
		  <img id="ercFig" src="figures/erc.svg" alt="erc">
		  <img id="desyFig" src="figures/logo_DESY.png" alt="desy">
		  <img id="euFig" src="figures/eu.svg" alt="eu">
		</div>
		
		<div>
		  <i>
			KSETA Plenary Workshop 2024
		  </i>
		</div>
	  </div>

	   <div id="GWPhysics" class="step" data-rel-x="1800" data-rel-to="title">
		<h2>Gravitational wave physics</h2>
	    <ul>
		  <li>Insights into many areas of fundamental (astro-)particle physics and cosmology. E.g.
			<ul>
			  <li>Near-horizon black hole physics</li>
			  <li>Internal structure of neutron stars</li>
			  <li>Tests of GR and maybe even quantum gravity</li>
			  <li>Dark matter (e.g. primordial black holes, axion clouds,...)</li>
			  <li>Formation of large scale structures</li>
			</ul>
		  </li>
		</ul>
		<p class="center">
		  <blockquote class="callout quote EN">
			I would like to mention astrophysics; in this field, the strange properties of the pulsars and quasars, and perhaps also the gravitational waves, can be considered as a challenge.
			<cite> - Werner Heissenberg</cite>
		  </blockquote>
		</p>
	  </div>

	  <div id="paramDetermination" class="step" data-rel-y="800" data-rel-to="GWPhysics">
		<h3><span class="h3span">Parameter determination</span></h3>
		<div class="ref"><a href="https://arxiv.org/pdf/2111.03606.pdf">[GWTC-3 LIGO & Virgo]</a></div>
		<div class="center">
		  <img id="massContoursFig" src="figures/massContours.png"></img>
		</div>
	  </div>

	  <div id="bank" class="step" data-rel-y="800" data-rel-to="paramDetermination">
		<h3><span class="h3span">Waveform template bank</span></h3>
		<div class="ref"><a href="https://arxiv.org/pdf/1904.01683.pdf">[Roulet et al. 2019]</a></div>
		<div class="center">
		  <img id="bankFig" src="figures/bank.png"></img>
		</div>
	  </div>
	  

	  <div id="GWPhysics2" class="step" data-rel-y="1000" data-rel-to="bank">
		<div class="center">
		  <img id="ligoFig" class="vanishPast" src="figures/ligo.png"></img>
		</div>
	  </div>

	  <div id="waveform1" class="step always" data-rel-z="-600"data-rel-to="GWPhysics2">
		<div class="center">
		  <img id="waveformFig" class="margin-bottom80" src="figures/waveform.svg"></img>
		</div>
	  </div>

	  <div id="waveform2" class="step vanishPast" data-rel-to="waveform1">
		<h3 class="vanishPast"><span class="h3span">Waveform modelling</span></h3>
		<div class="margin-bottom450">
		<div class="ref"><a href="">[EOB: Buonanno, Damour 1998]</a></div>
		</div>
		<div class="center">
		  <img id="waveform2Fig" src="figures/waveform2.svg"></img>
		</div>
	  </div>

	  <div id="target" class="step" data-rel-y="1000" data-rel-to="waveform2">
		<h3><span class="h3span">Target accuracy</span></h3>
		<div class="ref"><a href="https://journals.aps.org/prresearch/pdf/10.1103/PhysRevResearch.2.023151">[Pürrer, Haster 2020]</a></div>
		<div class="center">
		  <img id="mismatchFig" src="figures/mismatch.png"></img>
		  <ul>
		  <li>Mismatch needs to be reduced by three orders of magnitude for ET+CE</li>
		  </ul>
		</div>
	  </div>
	  
	  <div id="main" class="step" data-rel-x="1700" data-rel-to="GWPhysics">
		<h2><span class="orange">O</span>verview</h2>
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering"></img>
		</p>
	  </div>

	  <div id="B2B" class="step" data-rel-x="1700" data-rel-y="0">
		<h2>B<span class="orange">o</span>undary-To-Bound (B2B)</h2>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008, w/ Porto]</a><br/>
		<p>
		  Let us do some classical mechanics! Consider 2-body Hamiltonian in com coordinates
		  $$\cH \equiv \cH(\bp^2,\br^2) = E \,.$$
		  Invert
		  $$\bp^2\equiv\bp^2(\br^2,E)\,.$$
		  Motion is in a plane \(\rightarrow\) polar coordinates
		  $$\bp = p_r \hat{\br} + \frac{p_\phi}{r} \hat{\boldsymbol\phi}\,.$$

		</p>
	  </div>

	  <div id="B2B2" class="step" data-rel-x="0" data-rel-y="480">
		<p>
		  Identify the angular piece with the conserved angular momentum \(J=p_\phi\), leading to
		  $$\bp^2(\br^2,E) = p_r^2(r,E,J)+\frac{J^2}{\br^2}\,.$$
		  Finally, use Hamilton's equations
		  $$\begin{aligned}
		  \Delta \phi
		    &= \int \dd \phi
		    = \int\frac{\dd\phi}{\dd r}\dd r
		    = \int\frac{\dot\phi}{\dot r}\dd r
		    = \int\left(\frac{\partial \cH}{\partial p_\phi}\right)/\left(\frac{\partial \cH}{\partial p_r}\right)\dd r\\
		    &= \int\left(\frac{\partial \cH}{\partial \bp^2}\frac{\partial\bp^2}{\partial J}\right)/\left(\frac{\partial \cH}{\partial \bp^2}\frac{\partial \bp^2}{\partial p_r}\right)\dd r
		    = \int \frac{J}{r^2 p_r}\dd r
		  \end{aligned}$$
		</p>
	  </div>

	  <div id="B2B3" class="step" data-rel-x="0" data-rel-y="420">
		<p>
		  Periastron advance:
		  <div class="center">
			<div class="centerBox">
			  $$\Delta \Phi + 2\pi =  2J \int_{r_-}^{r_+} \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}$$
			</div>
		  </div>
		  Scattering angle (\(b=J/p_\infty\)):
		  <div class="center">
			<div class="centerBox">
			  $$\chi(J,E) +\pi = 2J \int_{r_{\rm min}}^\infty \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}$$
			</div>
		  </div>
		  It turns out that: \(r_-(J,E) = r_\mathrm{min}(J,E)\) and \(r_+(J,E) = r_\mathrm{min}(-J,E)\)
		</p>
	  </div>

	  <div id="B2B4" class="step" data-rel-x="590" data-rel-y="-55">
		$$\begin{align}
		&=2J \int_{r_\textrm{min}(J)}^{\infty} \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}\\
		&\quad-2J \int_{r_\textrm{min}(-J)}^{\infty} \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}\\
		\end{align}$$
	  </div>

	  <div id="B2B5" class="step" data-rel-x="-590" data-rel-y="380">
		<p>
		  Boundary-to-bound for the periastron advance
		  <div class="center">
			<div class="centerBox">
			  $$\Delta\Phi(J,E) = \chi(J,E) + \chi(-J,E)$$
			</div>
		  </div>
		</p>
	  </div>

	  <div id="B2B6" class="step" data-rel-x="0" data-rel-y="450">
		<h3><span class="h3span">Radiati<span class="orange">o</span>n</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2112.03976">[2112.03976, w/ Cho, Porto]</a>
		<p class="margin-top60">
		In a similar way we can analytically continue radiative observables, e.g. the energy and angular momentum loss
		$$\begin{align}
		\Delta E_\textrm{ell}(J,\cE) &= \Delta E_\textrm{hyp}(J,\cE)-\Delta E_\textrm{hyp}(-J,\cE)\\
		\Delta J_\textrm{ell}(J,\cE) &= \Delta J_\textrm{hyp}(J,\cE)+\Delta J_\textrm{hyp}(-J,\cE)\\
		\end{align}$$
		</p>
	  </div>

	  <div id="B2B7" class="step" data-rel-x="0" data-rel-y="600">
		<h3><span class="h3span">N<span class="orange">o</span>n-local-in-time contributions</span></h3>
		<p>Now here's a challenge:<br/>
		  Consider the effective two-body Hamiltonian
		  $$H = H^{\textrm{loc}} + H^{\textrm{nonloc}}$$
		  with
		  $$H^{\textrm{nonloc}}(t) \sim \mathrm{Pf} \int \frac{\dd t'}{|t-t'|}\mathcal{F}(r(t),r(t'))$$
		  Depends on full history of  \(r(t')\); different for bound and unbound systems!<br/>
		  \(\Rightarrow\) Cannot be obtained through B2B in the small-eccentricity limit.
	    </p>
	  </div>

	  <div id="B2B8" class="step" data-rel-x="0" data-rel-y="560">
		<a class="ref" href="https://arxiv.org/abs/2403.04853">[2403.04853, w/ Dlapa, Liu, Porto]</a>
		<p><b>Idea: </b></p>
		<ul>
		  <li>Compute nonloc pieces for unbound</li>
		  <li>Subtract from unbound result = local only</li>
		  <li>B2B local pieces only</li>
		  <li>Compute nonloc pieces for bound & add after B2B</li>
		</ul>
		<p>\(\Rightarrow\)Done @4PM</p>
	  </div>

	  <div id="pmeft" class="step" data-rel-x="1800" data-rel-y="0" data-rel-to="B2B">
		<h2>A w<span class="orange">o</span>rldline EFT framework</h2>
		<a class="ref" href="https://arxiv.org/abs/2006.01184">[GK, Porto 2006.01184]</a>
		<ul>
		  <li>Equivalent to solving <em>classical</em> equations of motion.</li>
		  <li>Perturbative expansion in \(G\): particle physics/amplitudes toolbox</li>
		  <li>EFT methodology: Full action \(\rightarrow\) effective action \(\rightarrow\) deflection/fluxes/waveform/...</li>
		  <li>Complete: allows inclusion of radiation, finite size, spin, \(n\)-body</li>
		</ul>
		<p class="center">
		  <img id="setupFig" src="figures/setup.svg" alt="setup"></img>
		</p>
	  </div>
	  
	  <div id="pmeft1.5" class="step" data-rel-x="0" data-rel-y="600">
		<h3><span class="h3span">Full the<span class="orange">o</span>ry</span></h3>
		<p>
		  Model the compact bodies by worldlines \(x_a^\mu(\tau)\) coupled to GR.
	      $$\begin{align}
		  S_{\rm EH} &= -2\Mp^2 \int \dd^4x \sqrt{-g} \, R[g]\\
		  S_{\rm pp} &= -\sum_a \frac{m_a}{2} \int \dd\tau_a\,  g_{\mu\nu}(x_{a}(\tau_a)) \dot{x}_{a}^\mu(\tau_a) \dot{x}_{a}^\nu (\tau_a)+\dots\\
		  \end{align}$$
		  We can add more terms to describe tidal and spin effects in an EFT style.
		</p>
	  </div>

	  <div id="pmeft2" class="step" data-rel-x="0" data-rel-y="580">
		<h3><span class="h3span">Effective two-body acti<span class="orange">o</span>n</span></h3>
		<p>
		  First step: Integrate out the gravitational field (classical saddlepoint).
		  $$g_{\mu\nu}=\eta_{\mu\nu} + \kappa h_{\mu\nu}$$
		  $$e^{i S_{\rm eff}[x_a] } = \int \cD h_{\mu\nu} \, e^{i S_{\rm EH}[h] + i S_{\rm GF}[h] + i S_{\rm TD}[h] + i S_{\rm pp}[x_a,h]}$$
		</p>
		<blockquote class="callout quote EN">
		  Don't use Feynman rules!</br>
		  <cite> - Every PhD supervisor in the field of amplitudes</cite>
		</blockquote>
		<p>
		Optimize the EH-Lagrangian by means of gauge-fixing terms and total derivatives:
		</p>
		<div class="container">
		<ul class="left">
		  <li>2-point Lagrangian: 2 terms</li>
		  <li>3-point Lagrangian: 6 terms</li>
		  <li>4-point Lagrangian: 18 terms</li>
		  <li>5-point Lagrangian: 36 terms</li>
		</ul>
		<img id="notBadFig" class="right" src="figures/notBad.png" alt="notBad"><img>
		</div>
	  </div>

	  <div id="rad" class="step" data-rel-x="0" data-rel-y="750">
		<h3><span class="h3span">Radiati<span class="orange">o</span>n reaction</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2207.00580">[GK, Neef, Porto 2207.00580]</a>
		<ul class="margin-top60">
		  <li>For causal motion: in-in formalism \(\longrightarrow\) doubling of fields
			$$\begin{align}
			\cS[h_1,h_2] = \cS_\textrm{EH}[h_1] - \cS_\textrm{EH}[h_2] -\sum_{A=1}^2 \frac{\kappa m_A}{2}\int\dd\tau_A &\left[h_{1,\mu\nu}(x_{1,A}(\tau_A))\dot{x}_{1,A}^\mu(\tau_A)\dot{x}_{1,A}^\nu(\tau_A)\right.\\
			&\quad\left.-h_{2,\mu\nu}(x_{2,A}(\tau_A))\dot{x}_{2,A}^\mu(\tau_A)\dot{x}_{2,A}^\nu(\tau_A)\right]
			\end{align}
			$$
		  </li>
		  <li>Rotate to Keldysh variables (advanced & retarded propagators)
			$$\begin{align}
			h_{\mu\nu}^- &= \frac{1}{2}(h_{1\mu\nu}+h_{2\mu\nu}) & h_{\mu\nu}^+ &= h_{1\mu\nu} - h_{2\mu\nu}\\
			x_{a,+}^\mu &= \frac{1}{2}(x_{a,1}^\mu+x_{a,2}^\mu) & x_{a,-}^\mu &= x_{a,1}^\mu - x_{a,2}^\mu
			\end{align}$$
		  </li>
		  <li>Seems impractical, but it's not!</li>
		</ul>
	  </div>

	  <div id="rad2" class="step" data-rel-x="0" data-rel-y="750">
		<ul>
		  <li>Simple Feynman rules for <em>variation</em> of the action (in the physical limit)
			<ul>
			  <li>\(\textrm{Feynman}\rightarrow\textrm{retarded/advanced}\) propagator; graviton vertices unchanged</li>
			  <li>Source:
				<div class="graphsRad">
				  <img id="sourceFig" src="figures/source.svg" alt="source"></img>
				  <div>
					$$ = -\frac{i  m}{2\Mp}\int\dd\tau \, e^{i\, k\cdot x} \dot{x}^\mu \dot{x}^\nu$$
				  </div>
				</div>
			  </li>
			  <li>Unique sink (hit by variation):
				<div class="graphsRad">
				  <img id="sinkFig" src="figures/sink.svg" alt="source"></img>
				  <div>
					$$=-\frac{i m}{2\Mp}  \int\dd\tau\,e^{i\,k\cdot x} \left[ i\, k^\alpha \dot{x}^\mu \dot{x}^\nu-i\,k\cdot \dot{x}\, \eta^{\mu\alpha}\dot{x}^\nu-\eta^{\mu\alpha}\ddot{x}^\nu-i\,k\cdot\dot{x}\, \eta^{\nu\alpha}\dot{x}^\mu-\eta^{\nu\alpha}\ddot{x}^\mu\right]$$
				  </div>
				</div>
			  </li>
			</ul>
		  </li>
		  <li>Variation of the eff. action
			<div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_{1,-}^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{1PM}=$$
		  </div>
		  <img id="graphsRad1Fig" src="figures/graphsRad1.svg" alt="graphsRad1"/>
    	<div class="marginLeft">
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_{1,-}^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{2PM}=$$
		  </div>
		  <img id="graphsRad2Fig" src="figures/graphsRad2.svg" alt="graphsRad2"/>
		  </div>
		  <div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_{1,-}^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{3PM}=$$
		  </div>
		  <img id="graphsRad3Fig" src="figures/graphsRad3.svg" alt="graphsRad3"/>
		  </div>
		  </li>
		</ul>
	  </div>
	  
	  <div id="pmeft3" class="step" data-rel-x="0" data-rel-y="850">
		<h3><span class="h3span">C<span class="orange">o</span>mputing observables</span></h3>
		Second step: Solve equation of motions
		$$\left.\frac{\delta \cS_{\textrm{eff}}[x_+,x_-]}{\delta x_{b,-}^\mu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}} = 0 \quad\Longleftrightarrow\quad m_b \ddot{x}_b^\mu(\tau) = \left.-\eta^{\mu\nu}\frac{\delta \cS_{\textrm{eff,int}}[x_+,x_-]}{\delta x_{b,-}^\nu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}}$$
		perturbatively, expanding around straight lines
		$$
		x_a^\mu(\tau) = b_a + u_a \tau + \sum_{n=1}^\infty G^n \delta^{(n)}x_a^\mu(\tau)
		$$
		Using above trajectories we can e.g. compute the deflection 
		$$\Delta p^\mu_1= m_1 \int_{-\infty}^{+\infty}\dd\tau \ddot{x}_1^\mu = - \eta^{\mu\nu}\int_{-\infty}^{+\infty} \dd\tau \left.\frac{\delta \cS_{\textrm{eff,int}}[x_-,x_+]}{\delta x_{1,-}^\nu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}}\,,$$
		or the change of the mechanical angular momentum
		$$\Delta J^{\mu\nu}_1 = m_1 \int_{-\infty}^{+\infty} \dd\tau ( x_1^\mu \ddot{x}_1^\nu- \ddot{x}_1^\mu x_1^\nu)\,.$$
	  </div>

	  <div id="int1" class="step" data-rel-x="1700" data-rel-to="pmeft">
		<h2>Integrati<span class="orange">o</span>n</h2>
		<ul>
		  <li>\(\tau\)-integrations are trivial to resolve: lead to delta functions and linear propagators.</li>
		</ul>
		Generic structure to any loop order (potential + radiation):
		$$
		\int \dd^Dq\frac{\delta(q\cdot u_1)\delta(q\cdot u_2)e^{i b\cdot q}}{(q^2)^m}
		\underbrace{\int \dd^D\ell_1\cdots\dd^D\ell_L\frac{\delta(\ell_1\cdot u_{a_1})\cdots\delta(\ell_L\cdot u_{a_L})}{(\ell_1\cdot u_{b_1}\pm i0)^{i_1}\cdots(\ell_L\cdot u_{b_L}\pm i0)^{i_L}(\textrm{sq. props})}}_{\textrm{Cut Feynman integrals with linear and square propagators}}
		$$
		<ul>
		  <li>Single scale integrals in \(\gamma = u_1\cdot u_2\), or \(\gamma=\frac{1}{2}\left(x+\frac{1}{x}\right)\)</li>
		  <li>We can use all modern integration methods from the amplitudes community</li>
		  <li>One delta function per loop \(\rightarrow\) half of the linear propagators are cut</li>
		  <li>Outer Fourier transform is easy.</li>
		</ul>
	  </div>

	  <div id="int2" class="step" data-rel-y="700" data-rel-x="0">
		<h3><span class="h3span">Integration pr<span class="orange">o</span>cedure</span></h3>
		<p class="center">
		  <img id="intProcfig" src="figures/integration.svg"></img>
		</p>
	  </div>

	  <div id="ibp" class="step vanishPast" data-rel-y="-100" data-rel-x="1100">
		<h4>Integration-by-parts relations</h4>
		$$\int \dd^D\ell_1\cdots\dd^D\ell_L \frac{\partial}{\partial\ell_i^\mu} v^\mu \frac{\delta(\ell_1\cdot u_{a_1})\cdots\delta(\ell_L\cdot u_{a_L})}{(\ell_1\cdot u_{b_1}\pm i0)^{i_1}\cdots(\ell_L\cdot u_{b_L}\pm i0)^{i_L}(\textrm{sq. props})}=0$$
		<ul>
		  <li>Relations among integrals with different propagator powers</li>
		  <li>Integrals form a finite-dimensional vector space</li>
		  <li>Every integral can be expressed in terms of a finite set of <em>Master Integrals (MIs)</em></li>
		  <li>Powerful tools to perform this reduction exist: FIRE6, Kira2, LiteRed2, Reduze, FiniteFlow</li>
		</ul>
	  </div>

	  <div id="int2Again" class="step" data-rel-y="0" data-rel-x="0" data-rel-to="int2">
	  </div>

	  <div id="DE" class="step" data-rel-y="50" data-rel-x="100" data-scale="0.5">
	  </div>

	  <div id="int2Again2" class="step" data-rel-y="0" data-rel-x="0" data-rel-to="int2">
	  </div>

	  <div id="int3" class="step" data-rel-y="700" data-rel-x="0">
		<h3><span class="h3span">Meth<span class="orange">o</span>d of regions @3PM</span></h3>
		<p class="center">
		  <img id="I1fig" src="figures/I1.png"></img>
		</p>
		<p>
		  $$I_1=\int_{\ell_1,\ell_2}\frac{\delta(\ell_1\cdot u_1)\delta(\ell_2\cdot u_2)}{\ell_1^2\ell_2^2(\ell_1+\ell_2-q)^2}$$
		</p>
	  </div>
	  
	  <div id="int5" class="step" data-rel-y="550" data-rel-x="0">
		<hr/>
		<p>
		  Can't parametrize causal propagators! Momentum space it is: \(k\equiv \ell_1+\ell_2-q\) and \(\ell\equiv\ell_2\)
		  $$\begin{align}
		  &\mathrm{potential:} & \ell&\sim (v_\infty,1)|{\bq}|\,, & k&\sim(v_\infty,1)|{\bq}|\,,\\
		  &\mathrm{radiation:} & \ell&\sim (v_\infty,1)|{\bq}|\,, & k&\sim(v_\infty,v_\infty)|{\bq}|\,.
		  \end{align}$$
		  Go to rest frame \(u_1=(1,0,0,0)\,, u_2=(\gamma, 0,0,v_\infty)\), \(v_\infty = \sqrt{\gamma^2-1}\):
		  $$\begin{align}
		  &\mathrm{potential:} \quad (\bell\to \bell, \bk\to \bk)\,,\\ 
		  &\mathrm{radiation:}  \quad (\bell\to \bell, \bk\to v_\infty \tilde\bk)\,,\quad \tilde\bk\sim \bq\,,\label{res3pm}
		  \end{align}$$
		  Leading to
		  $$\begin{align}
		  I_{1}^\mathrm{pot} &= -\int_{\bell,\bk} \frac{1}{[(\bk-\bell+\bq)^2]\, [\bell^2]\, [\bk^2]} + \cO(v_\infty^2)\,,
		  \\
		  I_{1}^\mathrm{rad} &= - \int_{\bell}  \frac{1}{  [(\bell - \bq)^2]\,[\bell^2] }\,
		  \int_{\tilde \bk} \frac{v_\infty^{d-2} }{  [\tilde\bk^2 - (\ell^z)^2] }
		  +\cO(v_\infty^{d})\,,
		  \end{align}$$
		</p>
	  </div>

	  <div id="int6" class="step" data-rel-y="700" data-rel-x="0">
		<hr/>
		<p>
		  Feynman vs. causal
		  $$\begin{align}
		  I_{1,\rm Fey}^\mathrm{rad} &= - \int_{\bell}  {1  \over  [(\bell - \bq)^2]\,[\bell^2] }\,
		  \int_{\bk} {v_\infty^{d-2} \over  [\bk^2 - (\ell^z)^2 - i0] }
		  +\cO(v_\infty^{d})\,,
		  \\[0.35 em]
		  I_{1,\rm ret}^\mathrm{rad} &= - \int_{\bell}  {1  \over  [(\bell - \bq)^2]\,[\bell^2] }\,
		  \int_{\bk} {v_\infty^{d-2} \over  [\bk^2 - (\ell^z + i0)^2] }
		  +\cO(v_\infty^{d})\,.
		  \end{align}$$
		</p>
		<p class="center"><img id="I1PNfig" src="figures/I1PN.png"></img></p>
		<p>
		  Compute recursively:
		  $$\begin{align}
		  \int_{\bk} {1 \over  [\bk^2 - (\ell^z)^2 \pm i0]} 
		  &= {\Gamma\big(1 {-} \tfrac{d}{2}\big) \over [-(\ell^z)^2 \pm i0]^{1- d/2}},
		  \\
		  \int_{\bk}{1 \over  [\bk^2 - (\ell^z \pm i0)^2]} 
		  &= {\Gamma\big(1 {-} \tfrac{d}{2}\big) \over [ - (\ell^z \pm i0)^2]^{1 - d/2}}
		  = {e^{\mp i \pi (d/2 - 1)} \Gamma\big(1 {-} \tfrac{d}{2}\big) \over  (\ell^z \pm i0)^{2 - d}}.
		  \end{align}$$
		</p>
		
	  </div>

	  <div id="int7" class="step" data-rel-y="580" data-rel-x="0">
		<hr/>
		<p>
		  Final results:
		  $$\begin{align}
		  I_{1}^\textrm{pot} &= -\,{\Gamma(3 - d)\, \Gamma^3\big(\frac{d}{2}-1\big)  \over  \Gamma\big(\frac{3 d}{2} - 3\big)}\,,
		  \\
		  %%
		  I_{1,\rm Fey}^\textrm{rad} &=  \frac{i^{-d}\, 2^{5-2d}\,  \Gamma(3-d)\, \Gamma\big(1-\frac{d}{2}\big)\, \Gamma(d-2)\,
		  \Gamma\big(\frac{d-1}{2}\big)}{\Gamma\big(d-\frac{3}{2}\big)}\, v_\infty^{d-2}
		  +\cO(v_\infty^{d})\,,
		  \\
		  I_{1,\rm ret}^\textrm{rad} &=  \,
		  \frac{ 8^{2-d}\,\sqrt{\pi}\, \Gamma^2\big(1-\frac{d}{2}\big)\, \Gamma (d-1)}{\Gamma\big(d - \frac{3}{2}\big)}\, v_{\infty }^{d-2}
		  + \cO(v_\infty^{d-1})\,.
		  \end{align}$$
		</p>
	  </div>

	  <div id="Dpres" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="int1">
		<h2>Results up t<span class="orange">o</span> 4PM</h2>
		<a class="ref" href="https://arxiv.org/abs/2210.05541">[Dlapa, GK, Liu, Neef, Porto 2210.05541]</a><br/>
		<div class="center">
		  $$\Delta^{(n)} p_1^\mu = c^{(n)}_{1b}\, \hat b^\mu +  \sum_{a} c^{(n)}_{1\check{u}_a}\, \check{u}_a^\mu$$
		  <img id="res12PMFig" src="figures/res12PM.png" alt="res12PM"/>
		</div>
	  </div>

	  <div id="Dpres1.5" class="step" data-rel-x="0" data-rel-y="520">
		<div class="center">
		  <img id="res3PMFig" src="figures/res3PM.png" alt="res3PM"/>
		</div>
	  </div>

	  <div id="Dpres1.9" class="step" data-rel-x="0" data-rel-y="700">
		<div class="center">
		  <img id="res4PMFig" src="figures/res4PM.png" alt="res4PM"/>
		</div>
	  </div>

	  <div id="FirsovNum1" class="step" data-rel-x="0" data-rel-y="700">
		<h3><span class="h3span">Firsov resummation </span></h3>
<div class="center">
		  <p class="inlineRef">Resummation and comparison to NR <a href="https://arxiv.org/abs/2211.01399">[Damour, Rettegno 2211.01399]</a></p>
		  <img id="chiEOBFig" src="figures/chiEOB.png"></img>
		</div>
	  </div>

	  <div id="FirsovNum2" class="step" data-rel-y="700" data-rel-to="Dpres2">
		<div class="center">
		  <img id="chiEOB2Fig" src="figures/chiEOB2.png"></img>
		</div>
	  </div>

	   <div id="conclusions" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="Dpres">
		<h2>Conclusi<span class="orange">o</span>ns</h2>
		<ul>
		  <li>WLEFT + B2B: systematic and efficient framework to study the gravitational 2-body dynamics.</li>
		  <li>Field theory techniques applied to PM integrals with causal propagators.</li>
		  <li>Complete answer, including radiation-reaction, for the impulse/deflection up to 4PM.</li>
		  <li>Local-nonlocal split: most accurate Hamiltonian from PM+PN data to date.</li>
		  <li>Resummed results agree well with numerical relativity simulations</li>
		</ul>
	  </div>

	  <div id="outlook" class="step" data-rel-x="1700" data-rel-y="0">
		<h2>Outlo<span class="orange">o</span>k</h2>
		<ul>
		  <li>5PM@1SF (conservative): 3 days ago by <a class="inlineRef" href="https://arxiv.org/abs/2403.07781">[Driesse et. al.]</a>, 5PM@2SF ???</li>
		  <li>Need to optimize integration tools for our purpose (e.g. causal/linear/delta propagators; single scale; FT).</li>
		  <li>How to simplify iterated elliptic integrals in non-local parts?</li>
		  <li>B2B for arbitrary spin and non-local PM contributions for quasi-circular orbits.</li>
		  <li>Other directions: spin, tidal deformations, angular momentum loss, waveform</li>
		</ul>
	  </div>

	  <div id="final" class="step" data-rel-x="0" data-rel-y="660">
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering">
		</p>
		<p class="small">
		  This research is supported by the ERC-CoG “Precision Gravity: From the LHC to LISA” provided by the European Research Council (ERC) under the European Union’s H2020 research and innovation programme (grant No. 817791), by the DFG under Germany’s Excellence Strategy ‘Quantum Universe’ (No. 390833306).
		</p>
	  </div>

	  <div id="Firsov" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="outlook">
		<h2>Firs<span class="orange">o</span>v's formula</h2>
		<a class="ref" href="https://arxiv.org/abs/1910.03008">[w/ Porto 1910.03008]</a><br/>
		Let us invert
		$$\chi(b,E) = -\pi + 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}} = \sum_{n=1} \chi^{(n)}_b(E) \left(\frac{GM}{b}\right)^n $$
		<a class="inlineRef" href="">[Firsov '53]</a>: dependence on \(r_\textrm{min}\) drops out
		<div class="center"><div class="centerBox">
			$$\bar\bp^2(r,E) = \exp\left[ \frac{2}{\pi} \int_{r|\bar{\bp}(r,E)|}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-r^2\bar\bp^2(r,E)}}\right] = 1 + \sum_{n=1}^\infty f_n(E) \left(\frac{GM}{r}\right)^n$$
		</div></div>
		These integrals are easy to perform in a PM-expanded form and one finds:
		$$\chi_b^{(n)} = \frac{\sqrt{\pi}}{2} \Gamma\left(\frac{n+1}{2}\right)\sum_{\sigma\in\mathcal{P}(n)}\frac{1}{\Gamma\left(1+\frac{n}{2} -\Sigma^\ell\right)}\prod_{\ell} \frac{f_{\sigma_{\ell}}^{\sigma^{\ell}}}{\sigma^{\ell}!}$$
		The inversion thereof also exists.
	  </div>

	  <div id="FirsovMagic" class="step" data-rel-x="0" data-rel-y="700">
		<h3><span class="h3span">PM to PN: The Magic <span class="orange">o</span>f Firsov</span></h3>
		<img id="firsovFig" src="figures/Firsov.jpg" alt="firsov">
		Let's consider a simple example
		$${\Delta \Phi}(j,\cE)= \sum_{n=1} \Delta \Phi_j^{(2n)}(\cE)/j^{2n}$$
		with \(j=J/(G M \mu)\). B2B states:
		$$\Delta \Phi_j^{(2n)}(\cE)= 4\, \chi^{(2n)}_j(\cE)$$
		Let's assume we don't know anything about \(\chi^{(4)}_j\). Does this mean we don't know anything about \(\Delta \Phi_j^{(4)}\)? There's lot of lower PN information that our PM results up to \(\chi_j^{(3)}\) should contain. Where are they?
	  </div>

	  <div id="FirsovMagic2" class="step" data-rel-x="0" data-rel-y="550">
		$$\frac{1}{4}\Delta \Phi_j^{(4)}= \chi^{(4)}_j = \left(\frac{p_\infty}{\mu}\right)^4\chi_b^{(4)}=\left(\frac{p_\infty}{\mu}\right)^4\frac{3\pi}{16}(2f_1f_3+f_2^2+2f_4)$$
		and in turn
		$$\begin{align}
		f_1&=2\chi_b^{(1)}\\
		f_2&=\frac{4}{\pi}\chi_b^{(2)}\\
		f_3&=\frac{1}{3}\left(\chi_b^{(1)}\right)^3+\frac{4}{\pi}\chi_b^{(1)}\chi_b^{(2)}+\chi_b^{(3)}
		\end{align}$$
		Prefectly reproduces 1 and 2PN information at order \(j^{-4}\).
	  </div>
	  
  	</div>

	<div id="impress-toolbar"></div>

	<!--
	<div class="hint">
      <p>Use a spacebar or arrow keys to navigate. <br/>
		Press 'P' to launch speaker console.</p>
	</div>
	-->
	<script>
	  if ("ontouchstart" in document.documentElement) { 
		  document.querySelector(".hint").innerHTML = "<p>Swipe left or right to navigate</p>";
	  }
	  </script>

	<script src="js/impress.js"></script>
	<script>impress().init();</script>

  </body>
</html>
